<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{homepromotions}prestashop>homepromotions_95615d2e1ed90fb7a17f4f03b1fec4e5'] = 'Muestra productos rebajados en la página inicial';
$_MODULE['<{homepromotions}prestashop>homepromotions_c8d9bc1ef5bd7db8eb177c7df73a046d'] = 'Muestra productos rebajados en la columna central de tu página inicial';
$_MODULE['<{homepromotions}prestashop>homepromotions_34a4fe4fe93fad3502c52fc4e7d935c1'] = 'Número introducido no válido.';
$_MODULE['<{homepromotions}prestashop>homepromotions_6af91e35dff67a43ace060d1d57d5d1a'] = 'Tus opciones han sido actualizadas.';
$_MODULE['<{homepromotions}prestashop>homepromotions_f4f70727dc34561dfde1a3c529b6205c'] = 'Opciones';
$_MODULE['<{homepromotions}prestashop>homepromotions_18d4c0f418e5354c069e4b81fe460ae0'] = 'Productos a mostrar';
$_MODULE['<{homepromotions}prestashop>homepromotions_67f1e5d51c61cde7fb40c89d9461979c'] = 'Establece el numero de productos rebajados que quieres mostrar en la página inicial.';
$_MODULE['<{homepromotions}prestashop>homepromotions_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{homepromotions}prestashop>homepromotions_b24344b6a2c4a94c9b0d0ebd4602aeca'] = 'Ver más';
$_MODULE['<{homepromotions}prestashop>homepromotions_d505d41279039b9a68b0427af27705c6'] = 'No hay productos rebajados en este momento.';
$_MODULE['<{homepromotions}prestashop>tab_70719754b29b359a20c297898ee618f8'] = 'Promociones';

{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- MODULE Home Promotions Products -->
{if isset($products) && $products}
    {include file="$tpl_dir./product-list.tpl" class='homepromotions tab-pane' id='homepromotions'}
    <p id="homepromotions-link" class="text-center" style="display: none;">
        <a class="button lnk_view btn btn-default" href="{$link->getPageLink('prices-drop', true)|escape:'html':'UTF-8'}">
            <span>{l s='View more' mod='homepromotions'}</span>
        </a>
    </p>
{literal}
    <script type="text/javascript">
    $(document).ready(function() {
        $("#home-page-tabs a").on('click', function(){
            console.log($(this));
            if ($(this).hasClass("homepromotions")) {
                $("#homepromotions-link").show();
            } else {
                $("#homepromotions-link").hide();
            }
        });
    });
    </script>
{/literal}
{else}
    <ul id="homepromotions" class="homepromotions tab-pane">
        <li class="alert alert-info">{l s='No featured products at this time.' mod='homepromotions'}</li>
    </ul>
{/if}
<!-- /MODULE Home Promotions Products -->
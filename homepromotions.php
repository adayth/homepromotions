<?php
/*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class HomePromotions extends Module
{
    protected static $cache_products;

	public function __construct()
	{
		$this->name = 'homepromotions';
		$this->tab = 'front_office_features';
		$this->version = '0.1';
		$this->author = 'Aday Talavera Hierro';
		$this->need_instance = 0;

		$this->bootstrap = true;
		parent::__construct();

		$this->displayName = $this->l('Show promotions on the homepage');
		$this->description = $this->l('Displays promotions in the central column of your homepage.');
	}

	public function install()
	{
        $this->_clearCache('*');
		Configuration::updateValue('HOME_PROMOTIONS_LENGTH', 8);

		if (!parent::install()
			|| !$this->registerHook('addproduct')
			|| !$this->registerHook('updateproduct')
			|| !$this->registerHook('deleteproduct')
			|| !$this->registerHook('categoryUpdate')
            || !$this->registerHook('header')
			|| !$this->registerHook('displayHomeTab')
			|| !$this->registerHook('displayHomeTabContent')
		)
			return false;

		return true;
	}

	public function uninstall()
	{
        $this->_clearCache('*');
		return parent::uninstall();
	}

    public function hookAddProduct($params) {
        $this->_clearCache('*');
    }

    public function hookUpdateProduct($params) {
        $this->_clearCache('*');
    }

    public function hookDeleteProduct($params) {
        $this->_clearCache('*');
    }

    public function hookCategoryUpdate($params) {
        $this->_clearCache('*');
    }

    public function _clearCache($template, $cache_id = NULL, $compile_id = NULL) {
        parent::_clearCache('homepromotions.tpl');
        parent::_clearCache('tab.tpl', 'homepromotions-tab');
    }

    public function _cacheProducts() {
        if (!isset(self::$cache_products)) {
            $products = self::getPricesDrop($this->context->language->id, 0, Configuration::get('HOME_PROMOTIONS_LENGTH'));
            self::$cache_products = $products;
        }

        if (self::$cache_products === false || empty(self::$cache_products))
            return false;
    }

    public function hookHeader($params) {
        if (isset($this->context->controller->php_self) && $this->context->controller->php_self == 'index') {
			$this->context->controller->addCSS(_THEME_CSS_DIR_.'product_list.css');
        }
        $this->context->controller->addCSS($this->_path.'css/homepromotions.css', 'all');
    }

    public function hookDisplayHomeTab($params)
	{
		if (!$this->isCached('tab.tpl', $this->getCacheId('homepromotions-tab'))) {
			$this->_cacheProducts();
        }

		return $this->display(__FILE__, 'tab.tpl', $this->getCacheId('homepromotions-tab'));
	}

	public function hookDisplayHome($params)
	{
		if (!$this->isCached('homepromotions.tpl', $this->getCacheId())) {
			$this->_cacheProducts();
			$this->smarty->assign(
				array(
					'products' => self::$cache_products,
					'add_prod_display' => Configuration::get('PS_ATTRIBUTE_CATEGORY_DISPLAY'),
					'homeSize' => Image::getSize(ImageType::getFormatedName('home')),
				)
			);
		}

		return $this->display(__FILE__, 'homepromotions.tpl', $this->getCacheId());
	}

    public function hookDisplayHomeTabContent($params) {
        return $this->hookDisplayHome($params);
    }

    public function getContent() {
        $output = '';

        $errors = array();
        if (Tools::isSubmit('submitHomePromotions')) {
            $lenth = (int) Tools::getValue('HOME_PROMOTIONS_LENGTH');
            if (!$lenth || $lenth <= 0 || !Validate::isInt($lenth)) {
                $errors[] = $this->l('An invalid number has been specified.');
            } else {
                Tools::clearCache(Context::getContext()->smarty, $this->getTemplatePath('homepromotions.tpl'));
                Configuration::updateValue('HOME_PROMOTIONS_LENGTH', (int) $lenth);
            }
            if (isset($errors) && count($errors))
                $output .= $this->displayError(implode('<br />', $errors));
            else
                $output .= $this->displayConfirmation($this->l('Your settings have been updated.'));
        }
        $output .= $this->renderForm();

        return $output;
    }

    public function renderForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('Products to show'),
						'name' => 'HOME_PROMOTIONS_LENGTH',
						'class' => 'fixed-width-xs',
						'desc' => $this->l('Set the number of promotions that you would like to display on homepage.'),
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->id = (int)Tools::getValue('id_carrier');
		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitHomePromotions';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}

	public function getConfigFieldsValues()
	{
		return array(
			'HOME_PROMOTIONS_LENGTH' => Tools::getValue('HOME_PROMOTIONS_LENGTH', Configuration::get('HOME_PROMOTIONS_LENGTH')),
		);
	}

    /**
     * Get prices drop (function taken from ProductCode class
     *
     * @param integer $id_lang Language id
     * @param integer $pageNumber Start from (optional)
     * @param integer $nbProducts Number of products to return (optional)
     * @param boolean $count Only in order to get total number (optional)
     * @return array Prices drop
     */
    public static function getPricesDrop($id_lang, $page_number = 0, $nb_products = 10, $count = false, $order_by = null, $order_way = null, $beginning = false, $ending = false, Context $context = null) {
        if (!Validate::isBool($count))
            die(Tools::displayError());

        if (!$context)
            $context = Context::getContext();
        if ($page_number < 0)
            $page_number = 0;
        if ($nb_products < 1)
            $nb_products = 10;
        if (empty($order_by) || $order_by == 'position')
            $order_by = 'price';
        if (empty($order_way))
            $order_way = 'DESC';
        if ($order_by == 'id_product' || $order_by == 'price' || $order_by == 'date_add' || $order_by == 'date_upd')
            $order_by_prefix = 'p';
        else if ($order_by == 'name')
            $order_by_prefix = 'pl';
        if (!Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way))
            die(Tools::displayError());
        $current_date = date('Y-m-d H:i:s');
        // Note: modified from original function
        //$ids_product = Product::_getProductIdByDate((!$beginning ? $current_date : $beginning), (!$ending ? $current_date : $ending), $context);
        $ids_product = self::_getProductIdByDate((!$beginning ? $current_date : $beginning), (!$ending ? $current_date : $ending), $context);

        $tab_id_product = array();
        foreach ($ids_product as $product)
            if (is_array($product))
                $tab_id_product[] = (int) $product['id_product'];
            else
                $tab_id_product[] = (int) $product;

        $front = true;
        if (!in_array($context->controller->controller_type, array('front', 'modulefront')))
            $front = false;

        $sql_groups = '';
        if (Group::isFeatureActive()) {
            // Note: Modification from original function
            //$groups = FrontController::getCurrentCustomerGroups();
            $groups = array();
            $sql_groups = 'AND p.`id_product` IN (
				SELECT cp.`id_product`
				FROM `' . _DB_PREFIX_ . 'category_group` cg
				LEFT JOIN `' . _DB_PREFIX_ . 'category_product` cp ON (cp.`id_category` = cg.`id_category`)
				WHERE cg.`id_group` ' . (count($groups) ? 'IN (' . implode(',', $groups) . ')' : '= 1') . '
			)';
        }

        if ($count) {
            return Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue('
			SELECT COUNT(DISTINCT p.`id_product`)
			FROM `' . _DB_PREFIX_ . 'product` p
			' . Shop::addSqlAssociation('product', 'p') . '
			WHERE product_shop.`active` = 1
			AND product_shop.`show_price` = 1
			' . ($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '') . '
			' . ((!$beginning && !$ending) ? 'AND p.`id_product` IN(' . ((is_array($tab_id_product) && count($tab_id_product)) ? implode(', ', $tab_id_product) : 0) . ')' : '') . '
			' . $sql_groups);
        }

        if (strpos($order_by, '.') > 0) {
            $order_by = explode('.', $order_by);
            $order_by = pSQL($order_by[0]) . '.`' . pSQL($order_by[1]) . '`';
        }

        $sql = '
		SELECT
			p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity, pl.`description`, pl.`description_short`,
			MAX(product_attribute_shop.id_product_attribute) id_product_attribute,
			pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`,
			pl.`name`, MAX(image_shop.`id_image`) id_image, il.`legend`, m.`name` AS manufacturer_name,
			DATEDIFF(
				p.`date_add`,
				DATE_SUB(
					NOW(),
					INTERVAL ' . (Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20) . ' DAY
				)
			) > 0 AS new
		FROM `' . _DB_PREFIX_ . 'product` p
		' . Shop::addSqlAssociation('product', 'p') . '
		LEFT JOIN ' . _DB_PREFIX_ . 'product_attribute pa ON (pa.id_product = p.id_product)
		' . Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.default_on=1') . '
		' . Product::sqlStock('p', 0, false, $context->shop) . '
		LEFT JOIN `' . _DB_PREFIX_ . 'product_lang` pl ON (
			p.`id_product` = pl.`id_product`
			AND pl.`id_lang` = ' . (int) $id_lang . Shop::addSqlRestrictionOnLang('pl') . '
		)
		LEFT JOIN `' . _DB_PREFIX_ . 'image` i ON (i.`id_product` = p.`id_product`)' .
                Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1') . '
		LEFT JOIN `' . _DB_PREFIX_ . 'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = ' . (int) $id_lang . ')
		LEFT JOIN `' . _DB_PREFIX_ . 'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)
		WHERE product_shop.`active` = 1
		AND product_shop.`show_price` = 1
		' . ($front ? ' AND p.`visibility` IN ("both", "catalog")' : '') . '
		' . ((!$beginning && !$ending) ? ' AND p.`id_product` IN (' . ((is_array($tab_id_product) && count($tab_id_product)) ? implode(', ', $tab_id_product) : 0) . ')' : '') . '
		' . $sql_groups . '
		GROUP BY product_shop.id_product
		ORDER BY ' . (isset($order_by_prefix) ? pSQL($order_by_prefix) . '.' : '') . pSQL($order_by) . ' ' . pSQL($order_way) . '
		LIMIT ' . (int) ($page_number * $nb_products) . ', ' . (int) $nb_products;

        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

        if (!$result)
            return false;

        if ($order_by == 'price')
            Tools::orderbyPrice($result, $order_way);

        return Product::getProductsProperties($id_lang, $result);
    }

    /**
     * Taken from ProductCore class
     * @param type $beginning
     * @param type $ending
     * @param Context $context
     * @param type $with_combination
     * @return type
     */
    protected static function _getProductIdByDate($beginning, $ending, Context $context = null, $with_combination = false) {
        if (!$context)
            $context = Context::getContext();

        $id_address = $context->cart->{Configuration::get('PS_TAX_ADDRESS_TYPE')};
        $ids = Address::getCountryAndState($id_address);
        $id_country = (int) ($ids['id_country'] ? $ids['id_country'] : Configuration::get('PS_COUNTRY_DEFAULT'));

        return SpecificPrice::getProductIdByDate(
                        $context->shop->id, $context->currency->id, $id_country, $context->customer->id_default_group, $beginning, $ending, 0, $with_combination
        );
    }

}
